#include <stdlib.h>
#include <stdio.h>
#include "pt_list.h"

struct sll_node *sll_create_node(short value, struct sll_node *next) {

    struct sll_node *new_node = malloc(sizeof(struct sll_node));

    new_node->value = value;
    new_node->next = next;

    return new_node;
}

void sll_free(struct sll_node *node) {
    if( node == NULL) return;
    if (node->next != NULL) {
        sll_free(node->next);
    }
    free(node);
}

void sll_print(struct sll_node *node) {
    if( node == NULL) return;
    sll_print(node->next);
    short x = node->value;
    if(x < 10) printf("00%i ", x);
    else if(x < 100) printf("0%i ", x);
    else printf("%i ", x);
}
