#include <stdio.h>
#include <stdlib.h>
#include "big_int.h"

int main() {
    char* number0 = "654565";
    struct big_int* result;
    int times = 10000;
    struct big_int * new_big_int = bi_gen(number0, getStrSize(number0), true);
    if (new_big_int == NULL){
        printf("can't create new big int");
    } else {
        printf("new_big_int ");
        bi_print(new_big_int);
        printf("increase by %i: ", times);
        for (int i = 0; i < times; ++i) {
            if (1 == bi_increase(new_big_int)) break;
        }
        bi_print(new_big_int);
        printf("decrease by %i: ", times);
        for (int i = 0; i < times; ++i) {
            if (1 == bi_decrease(new_big_int)) break;
        }
        bi_print(new_big_int);
    }

//    UNCOMMENT TO TAKE INPUT FROM STDIN
//    printf("\n***input e.g: +123 456 789\n");
//    char number1[1000];
//    char number2[1000];
//    printf("big_int1: ");
//    scanf("%s", number1);
//    printf("big_int2: ");
//    scanf("%s", number2);

    char* number1 = "-45465413216879841321987682165454646513456789387298237419847";
    char* number2 = "6465132163546513249875610329471098247301928";

    if (number1 == NULL || number2 == NULL) return 2;
    struct big_int* big_int1 = bi_gen(number1, getStrSize(number1), (number1[0]=='-')?false:true);
    struct big_int* big_int2 = bi_gen(number2, getStrSize(number2), (number2[0]=='-')?false:true);

//    for (int i = 0; i < 99; i++) big_int1 = bi_add(big_int1, big_int1);
//    for (int i = 0; i < 99; i++) big_int2 = bi_add(big_int2, big_int2);

    printf("\n");
    printf("big_int1 ");
    bi_print(big_int1);
    printf("big_int2 ");
    bi_print(big_int2);

    printf(" add      ");
    result = bi_add(big_int1, big_int2);
    bi_print(result);
    free(result);

    printf(" subtract ");
    result = bi_substract(big_int1, big_int2);
    bi_print(result);
    free(result);

    free(new_big_int);
    free(big_int1);
    free(big_int2);

    return 0;
}