//
// Created by hai on 7/3/17.
//

#include <malloc.h>
#include <stdlib.h>
#include "big_int.h"


char* get_digits(char* number, unsigned long length){
    char* c = calloc(length, sizeof(char));
    if (c == NULL) return NULL;
    unsigned long counter = 0;
    for (int i = 0; i < length; ++i) {
        char dig = number[i];
        if (dig >= '0' && dig <= '9'){
            c[counter] = dig;
            counter ++;
        }
    }
    return c ;
}

unsigned long getStrSize(char*str){
    if (str == "0") return 1;
    unsigned long size = 0;
    char c;
    do c = str[size++];
    while(c != '\0');
    return size-1;
}

int bi_append(struct big_int *big_int1, struct sll_node *new_node){
    big_int1->tail->next = new_node;
    big_int1->tail = new_node;
    return 0;
}

struct big_int* bi_gen(char*number, unsigned long length, bool positive){
    if (number == NULL) return NULL;
    number = get_digits(number, getStrSize(number));
    length = getStrSize(number);
    struct big_int* a = calloc(1, sizeof(struct big_int));
    if (a == NULL) return NULL;
    a->positive = positive;

    // save values to big int's new nodes
    struct sll_node* new_node;
    while(length > 0){
        char chunk[3] = {'0', '0', '0'};

        for (int i = 0; i < 3; ++i) {
            if (length==0) break;
            length-=1;
            chunk[3-i-1] = number[length];
        }
        new_node = sll_create_node((short )strtol(chunk, NULL, 10), NULL);
        if (new_node == NULL) {
            sll_free(a->head);
            free(a);
            return NULL;
        };
        if(a->head == NULL){
            a->head = new_node;
            a->tail = a->head;
        } else {
            bi_append(a, new_node);
        }
    }
    free(number);
    return a;
}

struct big_int* bi_gen_0(){
    struct big_int* a = malloc(sizeof(struct big_int));
    if (a == NULL) return NULL;
    a->positive = true;
    a->head = sll_create_node(0, NULL);
    a->tail = a->head;
    if (a->head == NULL) {
        free(a);
        return NULL;
    }
    return a;
}

int bi_increase(struct big_int * number){
    if (number == NULL) return 1;
    if(!(number->positive)){
        number->positive = true;
        bi_decrease(number);
        number->positive = false;
        return 0;
    }

    struct sll_node* current = number->head;

    current->value += 1;
    while(current->value > 999) {
        current->value = 0;
        if (current->next != NULL){
            current = current->next;
        } else {
            struct sll_node* new_node = sll_create_node(0, NULL);
            if (new_node == NULL) return 1;
            bi_append(number, new_node);
            current = number->tail;
        }
        current->value += 1;
    }
    return 0;
}

int bi_decrease(struct big_int * number){
    if (number == NULL) return 1;
    if (!(number->positive)){
        number->positive = true;
        bi_increase(number);
        number->positive = false;
        return 0;
    }
    struct sll_node* current = number->head;
    if (number->head != number->tail && number->tail->value == 0) remove_last(number);

    current->value -= 1;
    while(current->value < 0){
        if (current->next == NULL) {
            current->value = 1;
            number->positive = false;
            return 0;
        } else {
            current->value = 999;
            current = current->next;
            current->value -= 1;
        }
    }
    return 0;
}

struct big_int* bi_add(struct big_int * a, struct big_int* b){
    short carry = 0, chunk_value_a = 0, chunk_value_b = 0;
    struct big_int* result;

    if (!(a->positive) && !(b->positive)){  // add 2 negative numbers
        a->positive = true;
        b->positive = true;
        result = bi_add(a, b);
        result->positive = false;
        a->positive = false;
        b->positive = false;
        return result;
    }


    result = bi_gen_0();
    if (result == NULL) return NULL;
    struct sll_node* current_a = a->head;
    struct sll_node* current_b = b->head;
    while(current_a != NULL || current_b != NULL){
        carry = 0;
        chunk_value_a = (short) ((current_a == NULL) ? 0 : current_a->value);
        chunk_value_b = (short) ((current_b == NULL) ? 0 : current_b->value);
        chunk_value_a *= (a->positive)?1:-1;
        chunk_value_b *= (b->positive)?1:-1;
        result->tail->value += chunk_value_a + chunk_value_b;

        if (result->tail->value > 999){
            result->tail->value -= 1000;
            carry = 1;
        } else if (result->tail->value < 0){
            result->tail->value += 1000;
            carry = -1;
        }

        bi_append(result, sll_create_node(carry, NULL));
        if(current_a != NULL) current_a = current_a->next;
        if(current_b != NULL) current_b = current_b->next;
    }
    if (result->tail->value == 0) remove_last(result);
    if(result->tail->value == -1) {
        remove_last(result);
        struct big_int* x = find_x(result);
        x->positive = true;
        result->positive = false;
        result = bi_add(x, result);
        result->positive = false;
    }
    while (result->tail->value == 0) remove_last(result);
    return result;
}

struct big_int* bi_substract(struct big_int * a, struct big_int * b){
    struct big_int* result;
    b->positive = (b->positive)?false:true;
    result = bi_add(a, b);
    b->positive = (b->positive)?false:true;
    return result;
}

struct big_int* bi_multiply(struct big_int* a, struct big_int* b){
    bool positive_result = (a->positive == b->positive)?true:false;
    bool positive_a = a->positive;
    bool positive_b = b->positive;
    a->positive = true;
    b->positive = true;

    struct big_int* result = bi_gen_0();
    result = bi_add(result, a);

    while(b->head!= b->tail || b->head->value > 1 ){
        result = bi_add(result, a);
        bi_decrease(b);
    }

    a->positive = positive_a;
    b->positive = positive_b;
    result->positive = positive_result;
    return result;
}

unsigned long count_node(struct big_int* a){
    unsigned long counter = 1;
    struct sll_node* current = a->head;
    while(current != a->tail){
        current = current->next;
        counter += 1;
    }
    return counter;
}


void bi_print(struct big_int* a){
    if (a->positive) printf("+ ");
    else printf("- ");

    sll_print(a->head);
    printf("\n");
}

void remove_last(struct big_int* a){
    struct sll_node* current = a->head;
    if (current == NULL) return;
    while(current->next != a->tail){
        current = current->next;
    }

    sll_free(current->next);
    current->next = NULL;
    a->tail = current;
}

struct big_int* find_x(struct big_int* a){
    struct big_int* new = bi_gen_0();
    struct sll_node* current = a->head;
    while(current->next != NULL){
        current = current->next;
        bi_append(new, sll_create_node(0, NULL));
    }
    if (current->value > 99){
        bi_append(new, sll_create_node(1, NULL));
    }else if (current->value >9){
        new->tail->value = 100;
    }else{
        new->tail->value = 10;
    }
    return new;
}