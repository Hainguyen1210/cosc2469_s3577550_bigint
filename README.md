# This program is my implementation of big int #
* the data structure used in this implementation is the singly linked list
* big int is divided into chunks and saved in nodes, the data type to hold each chunk is short (2 bytes)
* the program currently contains 4 main functions: increment, decrement, addition, subtraction. The algorithm to perform addition is "Grade school".
* for more detailed documentation and cost analysis, please read the file "big_int.h"