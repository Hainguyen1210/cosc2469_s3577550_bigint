#ifndef PT_LIST
#define PT_LIST

/** 
 * Singly Linked List Node
 */
struct sll_node {
    short value;
    struct sll_node *next;  /**< pointer to the next node in the list */
};

struct sll_node *sll_create_node( short value, struct sll_node *next);

void sll_insert_after(struct sll_node *new_node, struct sll_node *node);

void sll_free(struct sll_node *node);

void sll_print(struct sll_node *node);


#endif
