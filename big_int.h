//
// Created by hai on 7/3/17.
//
#include <stdbool.h>
#import "pt_list.h"

#ifndef BIG_INT_BIG_INT_H
#define BIG_INT_BIG_INT_H

/*
 * the algorithm uses singly linked list to construct a big int,
 * each number is divided into chunks(nodes) from right to left
 * head is the least significant chunk and the tail is the most significant one
 * each chunk is 3-digits number. e.g this number 12345678 is divided by 3 chunks: (tail) 012 <- 345 <- 678 (head)
 *
 * the values range of each chunk is from 0 to 999
 *      but stored in a short variable (2 bytes)
 *      it is not efficient in memory but it might easier to implement and print out
 * the whole big_int has a boolean to indicate whether positive or negative
 */
typedef struct big_int {
    bool positive;
    struct sll_node* head;
    struct sll_node* tail;
};

/*
 * function initialize big int struct, assign its sign.
 * function reads the input string and divide it into chunks
 * and appends these chunks to the tail of the big int.
 */
struct big_int* bi_gen(char* number, unsigned long length, bool positive);
//int bi_save(struct big_int * a, short* number, unsigned long length, bool positive);

/*
 * append one more chunk to the end of current big int as the number grows
 */
int bi_append(struct big_int *big_int1, struct sll_node *new_node);

/*
 * add 1 to the least significant chunk
 * keep resetting(set to 0) the current chunk and carrying 1 to the next chunk until it not full (>999).
 * create one more chunk if needed
 *
 * note: if the number is negative, redirect to the decrease function with positive sign
 *
 * COST ANALYSIS: n is the number of nodes/chunks
 * if number negative => toggle sign twice +2
 * assign current node for looping +1
 * add 1 to the head chunk/node + 1
 * - best case: there is no carry +0
 * - worst case: carry all the way and append 1 more node + 2n + 6
 * - average case: carry half way +n
 *
 * ==>
 *  best case: constant
 *  worst case: O(n)
 *  average case: O(n)
 */
int bi_increase(struct big_int * number);

/*
 * subtract 1 from the least significant chunk
 * keep resetting(set to 999) the current chunk, moving to the next chunk and subtract 1
 * remove the last chunk if needed
 *
 * note: if the number is negative, redirect to the increase function with positive sign
 *
 *
 * COST ANALYSIS: n is the number of nodes/chunks
 * if number negative => toggle sign twice +2
 * assign current node for looping +1
 * if there is redundant tail node => remove
 *  - best case: there is no redundant node +0
 *  - worst case: there is redundant node   +n+1
 * subtract 1 from the head chunk +1
 *  - best case: there is no carry  +0
 *  - worst case: carry all the way +3n
 *  - average case: carry half way  +1.5n
 *
 * ==>
 *  best case: constant
 *  worst case: O(n)
 *  average case: O(n)
 */
int bi_decrease(struct big_int * a);

/*
 * e.g: a = 012 345 678
 *      b = 123 456 789
 * create new number to store the result
 * loop through each chunk of 2 numbers from the least significant one. e.g: 678 and 789
 * perform built-in addition on these selected chunks and save the result to the corresponding chunk of result
 * if the result is larger 999 subtract 1000 and carry 1 to the next created chunk (e.g result = 1 467)
 * keep doing until reach the tail node of both number.
 * if number a has less nodes than number b, append those nodes from b to result.
 * if the last carry out is -1, minus the result by the closest 10s number
 *      e.g: -23 + 72 => result = 51, carry -1 -> result = 100 - 51 = 49
 *  return the result
 *
 * COST ANALYSIS: n is the number of nodes/chunks of the larger number
 * initialize variables +4
 * if it's the addition of 2 negative numbers transform to the addition of 2 positive numbers +5
 * generate result number +5
 * assign current nodes for looping +2
 * perform addition through out all the chunks/nodes +10n
 * if there is redundant tail node => remove
 *  - best case: there is no redundant node +0
 *  - worst case: there is redundant node   +n+1
 * if the last carry is -1
 *  - best case: not happen +0
 *  - worst case: 5n + 9
 * if there is redundant tail node => remove
 *  - best case: there is no redundant node +0
 *  - worst case: there is redundant node   +n+1
 *
 *  ==>
 *   best case: O(n)
 *   worst case: O(n)
 *   no average case
 */
struct big_int* bi_add(struct big_int * a, struct big_int* b);

/*
 * the subtraction is basically based on addition
 * convert the subtraction of a and b to the addition of a and -b
 *
 * COST ANALYSIS: n is the number of nodes/chunks of the larger number
 * the cost of this function is 4 more than addition
 *
 *
 *  ==>
 *   best case: O(n)
 *   worst case: O(n)
 *   no average case
 */
struct big_int* bi_substract(struct big_int * a, struct big_int * b);

unsigned long count_node(struct big_int* a);

void bi_print(struct big_int* a);

void remove_last(struct big_int* a );

struct big_int* find_x(struct big_int* a);

unsigned long getStrSize(char*str);

#endif //BIG_INT_BIG_INT_H
